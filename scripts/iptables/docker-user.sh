#!/bin/bash

sudo iptables -F DOCKER-USER
sudo iptables -A DOCKER-USER -i lo -j RETURN
sudo iptables -A DOCKER-USER -i docker0 -j RETURN
sudo iptables -A DOCKER-USER -i br-+ -j RETURN
sudo iptables -A DOCKER-USER -m state --state ESTABLISHED,RELATED -j RETURN
sudo iptables -A DOCKER-USER -j DROP
