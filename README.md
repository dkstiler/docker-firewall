# docker-firewall

Linux firewall for docker using systemd and iptables

# Table of Contents
[[_TOC_]]

# About

Docker implements networking and sandboxing in Linux with iptables. When containers are running Docker places a rule in the NAT table's PREROUTING chain to catch network packets matching ports exposed by Docker containers. 

A sometimes surprising consequence is that even if you've configured your iptables rules to DROP packets on your INPUT table your exposed Docker containers are still accessible. This package ensures that your exposed Docker containers are only accessible locally to your Docker host and other containers, and not to other hosts on your network.

# Install

```
git clone https://gitlab.com/cyber5k/docker-firewall.git
./docker-firewall/install.sh
```

# How it works

- *Firewall*: Rules are added to the DOCKER-USER table to limit access to docker containers from loopback, docker0, and bridged networks
- *Systemd*: Ensure these rules are added after Docker starts
