#!/bin/bash

set -e

sudo git clone https://gitlab.com/cyber5k/docker-firewall.git /opt/docker-firewall
sudo cp /opt/docker-firewall/scripts/services/docker-firewall.service /etc/systemd/system/
sudo systemctl start docker-firewall
sudo systemctl enable docker-firewall
